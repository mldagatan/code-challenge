require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)

if Rails.env.production?
  abort('The Rails environment is running in production mode!')
end

require 'rspec/rails'
require 'capybara/rails'
require 'webmock/rspec'
require 'simplecov'
require 'database_cleaner'

WebMock.disable_net_connect!(allow_localhost: true)

ActiveRecord::Migration.maintain_test_schema!

Dir['./spec/support/**/*.rb'].each { |f| require f }

RSpec.configure do |config|
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.use_transactional_fixtures = true

  config.infer_spec_type_from_file_location!

  config.filter_rails_from_backtrace!

  config.include ActionView::TestCase::Behavior, file_path: %r{spec/presenters}

  config.include Request::JsonHelpers, type: :request
end
