FactoryBot.define do
  factory :shipment do
    shipment_date { Date.today + 1.day }
    status        'ready'

    after(:create) do |shipment|
      robots = create_list(:robot, 10)

      shipment.robots << robots
    end
  end
end
