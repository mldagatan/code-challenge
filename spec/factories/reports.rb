FactoryBot.define do
  factory :report do
    robot      { Robot.first || association(:robot) }
    conclusion 'recycle'
  end
end
