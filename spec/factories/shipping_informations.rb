FactoryBot.define do
  factory :shipping_information do
    robot     { Robot.first || association(:robot) }
    shipment  { Shipment.first || association(:shipment) }
    status    'pending'
  end
end
