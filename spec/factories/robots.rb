FactoryBot.define do
  factory :robot do
    name     { Faker::Superhero.name }
    statuses { Robot::STATUSES.sample(3) }

    after(:build) do |robot|
      robot.configuration = {
        hasSentience: [true, false].sample,
        hasWheels: [true, false].sample,
        hasTracks: [true, false].sample,
        numberOfRotors: rand(1..10),
        colour: Faker::Color.color_name
      }
    end
  end
end
