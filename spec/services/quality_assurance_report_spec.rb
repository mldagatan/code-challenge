require 'rails_helper'

describe QualityAssuranceReport do
  let!(:for_recycle) { create_list(:robot, 10) }
  let!(:factory_seconds) { create_list(:robot, 9) }
  let!(:passed) { create_list(:robot, 8) }

  let(:payload) do
    {
      recycleRobots: for_recycle.pluck(:id),
      factorySeconds: factory_seconds.pluck(:id),
      passedQA: passed.pluck(:id)
    }
  end

  let!(:qa) { described_class.new(payload) }

  describe '.process' do
    describe 'robot hash' do
      subject do
        JSON.parse(qa.process.to_json, symbolize_names: true).first
      end

      it { is_expected.to have_key(:id) }
      it { is_expected.to have_key(:report) }
    end

    describe 'report hash' do
      subject do
        JSON.parse(qa.process.to_json, symbolize_names: true).first[:report]
      end

      it { is_expected.to have_key(:robot_id) }
      it { is_expected.to have_key(:conclusion) }
    end

    context 'when process executes' do
      before do
        qa.process
      end

      it 'size of reports' do
        expect(Report.all.size).to eq 27
      end

      it 'creates reports for the robots' do
        robot_ids_from_reports = Report.all.pluck(:robot_id)
        robot_ids = (for_recycle + factory_seconds + passed).pluck(:id)

        expect(robot_ids_from_reports).to match_array(robot_ids)
      end

      it 'assigns report conclusions recycled' do
        all_recycled = true

        for_recycle.each do |robot|
          all_recycled = false if robot.report.conclusion != 'recycle'
        end

        expect(all_recycled).to eq true
      end

      it 'assigns report conclusions passed' do
        all_passed = true

        passed.each do |robot|
          all_passed = false if robot.report.conclusion != 'passed'
        end

        expect(all_passed).to eq true
      end

      it 'assigns report conclusions factory seconds' do
        all_seconds = true

        factory_seconds.each do |robot|
          all_seconds = false if robot.report.conclusion != 'factory second'
        end

        expect(all_seconds).to eq true
      end
    end
  end
end
