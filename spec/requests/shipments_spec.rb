require 'rails_helper'

describe 'Shipments API' do
  let!(:shipments) { create_list(:shipment, 5) }
  let!(:robot_ids) { create_list(:robot, 10).pluck(:id) }
  let!(:shipments_json) do
    JSON.parse(shipments.to_json(methods: :robot_ids), symbolize_names: true)
  end

  describe 'GET /api/shipments.json' do
    before do
      get '/api/shipments.json'
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a list of shipments with robot ids' do
      expect(json).to match_array(shipments_json)
    end
  end

  describe 'PUT /api/shipments/create.json' do
    before do
      params = { robot_ids: robot_ids, shipment_date: Date.today + 1.day }

      put '/api/shipments/create.json', params: params
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    describe 'shipment hash' do
      subject { json }

      it { is_expected.to have_key(:id) }
      it { is_expected.to have_key(:uuid) }
      it { is_expected.to have_key(:shipment_date) }
      it { is_expected.to have_key(:robot_ids) }
    end

    describe 'if no shipment date' do
      before do
        params = { robot_ids: robot_ids }
        put '/api/shipments/create.json', params: params
      end

      it 'returns status 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns errors' do
        expect(json).to have_key(:errors)
      end
    end
  end
end
