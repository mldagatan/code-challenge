require 'rails_helper'

describe 'Robots API' do
  let!(:for_recycle) { create_list(:robot, 10) }
  let!(:factory_seconds) { create_list(:robot, 9) }
  let!(:passed) { create_list(:robot, 8) }
  let!(:robots) { for_recycle + factory_seconds + passed }
  let!(:robot) { Robot.find(robots.first.id) }

  describe 'GET /api/robots.json' do
    before do
      get '/api/robots.json', headers: { 'Content-Type': 'application/json' }
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns a list of robots' do
      robots_json = JSON.parse(robots.to_json, symbolize_names: true)

      expect(json).to eq(robots_json)
    end

    it 'returns a limited list of robots' do
      get '/api/robots.json', headers: { 'Content-Type': 'application/json' },
                              params: { limit: 5 }

      expect(json.size).to eq(5)
    end

    describe 'robot hash' do
      subject { json.first }

      it { is_expected.to have_key(:id) }
      it { is_expected.to have_key(:name) }
      it { is_expected.to have_key(:configuration) }
      it { is_expected.to have_key(:statuses) }
    end

    describe 'robot configuration hash' do
      subject { json.first[:configuration] }

      it { is_expected.to have_key(:colour) }
      it { is_expected.to have_key(:hasWheels) }
      it { is_expected.to have_key(:hasTracks) }
      it { is_expected.to have_key(:hasSentience) }
      it { is_expected.to have_key(:numberOfRotors) }
    end
  end

  describe 'GET /api/robots/for_qa.json' do
    before do
      get '/api/robots/for_qa.json'
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'POST /api/robots/:id/extinguish.json' do
    before do
      robot.statuses << 'on fire' unless robot.statuses.include?('on fire')

      post "/api/robots/#{robot.id}/extinguish.json"
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'removes on fire status from robot' do
      robot.reload

      expect(robot.statuses).not_to include('on fire')
    end

    it 'returns a json with the robot object' do
      robot.reload
      robot_json = JSON.parse(robot.to_json, symbolize_names: true)

      expect(json[:robot]).to eq(robot_json)
    end
  end

  describe 'POST /api/robots/quality_assurance.json' do
    before do
      payload = {
        recycleRobots: for_recycle.pluck(:id),
        factorySeconds: factory_seconds.pluck(:id),
        passedQA: passed.pluck(:id)
      }

      post '/api/robots/quality_assurance.json', params: { payload: payload }
    end

    it 'returns status 200' do
      expect(response).to have_http_status(200)
    end

    it "returns qa'd robots" do
      robot_ids_from_reports = Report.all.pluck(:robot_id)

      expect(robot_ids_from_reports).to match_array(json.pluck(:id))
    end

    describe 'report hash' do
      subject { json.first[:report] }

      it { is_expected.to have_key(:robot_id) }
      it { is_expected.to have_key(:conclusion) }
    end
  end
end
