require 'rails_helper'

describe ShippingInformation do
  let(:shipping_information) { build(:shipping_information) }

  it 'has a valid factory' do
    expect(shipping_information).to be_valid
  end

  describe 'validations' do
    it { expect(shipping_information).to validate_presence_of(:robot_id) }
    it { expect(shipping_information).to validate_presence_of(:shipment_id) }
    it { expect(shipping_information).to validate_presence_of(:status) }

    it do
      expect(shipping_information).to \
        validate_uniqueness_of(:robot_id).scoped_to(:shipment_id)
    end

    it do
      expect(shipping_information).to \
        validate_inclusion_of(:status).in_array(ShippingInformation::STATUSES)
    end
  end

  describe 'associations' do
    it { expect(shipping_information).to belong_to(:robot) }
    it { expect(shipping_information).to belong_to(:shipment) }
  end
end
