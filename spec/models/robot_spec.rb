require 'rails_helper'

describe Robot do
  let(:robot) { build(:robot) }

  it 'has a valid factory' do
    expect(robot).to be_valid
  end

  describe 'validations' do
    it { expect(robot).to validate_presence_of(:name) }

    context 'when statuses is included in Robot::STATUSES' do
      it { expect(robot).to be_valid }
    end

    context 'when statuses is not included in Robot::STATUSES' do
      it do
        robot.statuses.push('not a valid status')
        expect(robot).not_to be_valid
      end
    end
  end

  describe 'associations' do
    it { expect(robot).to have_one(:shipping_information) }
    it { expect(robot).to have_one(:shipment).through(:shipping_information) }
    it { expect(robot).to have_one(:report) }
  end

  describe 'scopes' do
    before do
      robot.save
    end

    it '.for_qa returns all robots without a report' do
      expect(described_class.for_qa.first).to eq(robot)
    end
  end
end
