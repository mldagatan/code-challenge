require 'rails_helper'

describe Shipment do
  let(:shipment) { build(:shipment) }

  it 'has a valid factory' do
    expect(shipment).to be_valid
  end

  describe 'validations' do
    it { expect(shipment).to validate_presence_of(:status) }
    it { expect(shipment).to validate_presence_of(:shipment_date) }

    it do
      expect(shipment).to \
        validate_inclusion_of(:status).in_array(Shipment::STATUSES)
    end
  end

  describe 'associations' do
    it { expect(shipment).to have_many(:shipping_informations) }

    it do
      expect(shipment).to have_many(:robots).through(:shipping_informations)
    end
  end
end
