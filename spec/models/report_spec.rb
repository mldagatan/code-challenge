require 'rails_helper'

describe Report do
  let(:report) { build(:report) }

  it 'has a valid factory' do
    expect(report).to be_valid
  end

  describe 'validations' do
    it { expect(report).to validate_presence_of(:robot_id) }
    it { expect(report).to validate_presence_of(:conclusion) }

    it do
      expect(report).to \
        validate_inclusion_of(:conclusion).in_array(Report::CONCLUSIONS)
    end
  end

  describe 'associations' do
    it { expect(report).to belong_to(:robot) }
  end
end
