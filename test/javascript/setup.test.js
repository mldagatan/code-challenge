import React from 'react';
import { mount } from 'enzyme';

import App from 'App';
import Root from 'Root';
import routes from 'routes';

let component;

beforeEach(() => {
  component = mount(<App />);
});

it('renders routes', () => {
  expect(component.find(routes)).toHaveLength(1);
});

it('renders Root', () => {
  expect(component.find(Root)).toHaveLength(1);
});
