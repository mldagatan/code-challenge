import React from 'react';
import { mount } from 'enzyme';
import moxios from 'moxios';

import App from 'App';
import mockContent from 'mockContent';
import RobotsList from 'containers/RobotsList/RobotsList';

let component;

beforeEach(() => {
  moxios.install();
  moxios.stubRequest('/api/robots.json', {
    status: 200,
    response: mockContent
  });

  component = mount(<App />);
});


afterEach(() => {
  component.unmount();
});

it('renders the robots list', () => {
  expect(component.find(RobotsList)).toHaveLength(1);
});

describe('the robot list', () => {
  it ('can fetch a list of robots and display them', () => {
    moxios.wait((done) => {
      component.update();

      expect(component.find('.robot-row')).toHaveLength(3);

      done();
    });
  });

  it ('adds a robot to qa list on add qa click', () => {
    moxios.wait((done) => {
      component.update();

      component.find('.robot-row .report button').first().simulate('click');

      component.update();

      expect(component.find('.robot-row .report').first().render().text())
        .toContain('Listed in QA');

      done();
    });
  });
});
