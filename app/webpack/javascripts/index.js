import Rails from 'rails-ujs';
import toastr from 'toastr';
import * as ActiveStorage from 'activestorage';
import './react';

Rails.start();
ActiveStorage.start();

function requireAll(r) {
  r.keys().forEach(r);
}

requireAll(require.context('./behaviors/', true, /\.js$/));
