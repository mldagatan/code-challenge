import axios from 'axios';
import {
  ROBOTS_FETCH,
  ADD_ROBOT_TO_QA,
  TOGGLE_NAV,
  REMOVE_ROBOT_FROM_QA,
  EXTINGUISH_ROBOT,
  PROCESS_QUALITY_ASSURANCE,
  SHIPMENTS_FETCH,
  CLEAR_QA,
  ADD_TO_MANIFEST,
  REMOVE_FROM_MANIFEST,
  CLEAR_MANIFEST,
  CREATE_SHIPMENT
} from './actionTypes';

export const fetchRobots = () => {
  const request = axios.get('/api/robots.json');

  return {
    type: ROBOTS_FETCH,
    payload: request
  };
};

export const addRobotToQA = (id) => ({
  type: ADD_ROBOT_TO_QA,
  payload: id
});

export const removeRobotFromQA = (id) => ({
  type: REMOVE_ROBOT_FROM_QA,
  payload: id
});

export const toggleNav = () => ({
  type: TOGGLE_NAV,
  payload: null
});

export const extinguishRobot = (id) => {
  const request = axios.post(`/api/robots/${id}/extinguish.json`);

  return({
    type: EXTINGUISH_ROBOT,
    payload: request
  })
}

export const processQA = (payload) => {
  const request = axios.post(`/api/robots/quality_assurance.json`,
    { payload }
  );

  return({
    type: PROCESS_QUALITY_ASSURANCE,
    payload: request
  })
}

export const fetchShipments = () => {
  const request = axios.get('/api/shipments.json');

  return({
    type: SHIPMENTS_FETCH,
    payload: request
  });
}

export const clearQA = () => ({
  type: CLEAR_QA,
  payload: null
});

export const addToManifest = (id) => ({
  type: ADD_TO_MANIFEST,
  payload: id
})

export const removeFromManifest = (id) => ({
  type: REMOVE_FROM_MANIFEST,
  payload: id
})

export const clearManifest = () => ({
  type: CLEAR_MANIFEST,
  payload: null
})

export const createShipment = (manifest) => {
  const request = axios.put(`/api/shipments/create.json`, {
    robot_ids: manifest,
    shipment_date: new Date()
  });

  return({
    type: CREATE_SHIPMENT,
    payload: request
  });
};
