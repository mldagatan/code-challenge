const drawerWidth = 300;

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    background: '#fff',
    [theme.breakpoints.up('lg')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  link: {
    color: 'rgba(0, 0, 0, 0.54) !important',
    textDecoration: 'none',

    '& svg': {
      fontSize: 20
    }
  },
  toolBar: {
    minHeight: 45
  }
});

export default styles;
