import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import LocalShipping from '@material-ui/icons/LocalShipping';
import Add from '@material-ui/icons/Add';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import Badge from '@material-ui/core/Badge';
import styles from './styles';

const Header = ({ classes, handleOnClick, robotsQA = [], manifest = [] }) => {
  const qaSize = robotsQA.length;
  const manifestSize = manifest.length;

  const QaButton = () => {
    if (qaSize) {
      return(
        <IconButton color="inherit" aria-label="Shipments">
          <Badge color="error" badgeContent={qaSize}>
            <LibraryBooks />
          </Badge>
        </IconButton>
      );
    }
    return(
      <IconButton color="inherit" aria-label="Shipments">
        <LibraryBooks />
      </IconButton>
    );
  };

  const NewShipmentButton = () => {
    if (manifestSize) {
      return(
        <IconButton color="inherit" aria-label="Manifest">
          <Badge color="error" badgeContent={manifestSize}>
            <Add />
            <LocalShipping />
          </Badge>
        </IconButton>
      )
    }

    return(
      <IconButton color="inherit" aria-label="Manifest">
        <Add />
        <LocalShipping />
      </IconButton>
    );
  }

  const BurgerIcon = () => {
    if (manifestSize || qaSize) {
      const total = manifestSize + qaSize

      return(
        <Badge color="error" badgeContent={total}>
          <MenuIcon />
        </Badge>
      )
    }

    return(
      <MenuIcon />
    )
  }

  return(
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar} color="default">
        <Toolbar className={classes.toolBar}>

          <IconButton
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
            onClick={handleOnClick}
          >
            <BurgerIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            <Link to="/" className={classes.link}>
              Robots
            </Link>
          </Typography>

          <Link to="/qa" className={classes.link}>
            <QaButton />
          </Link>

          <Link to="/shipments/new" className={classes.link}>
            <NewShipmentButton />
          </Link>

        </Toolbar>
      </AppBar>
    </div>
  )
};

Header.defaultProps = {
  robotsQA: []
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  robotsQA: PropTypes.array,
  handleOnClick: PropTypes.func.isRequired
};

Header.defaultProp = {
  handleOnClick: () => {},
  robotsQA: [],
  classes: {}
};

const mapStateToProps = ({ robotsQA, manifest }) => ({ robotsQA, manifest });
const withstyles = withStyles(styles)(Header);

export default connect(mapStateToProps)(withstyles);
