import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import LocalShipping from '@material-ui/icons/LocalShipping';
import Android from '@material-ui/icons/Android';
import Badge from '@material-ui/core/Badge';

import { toggleNav } from '../../actions';

import Header from '../Header/Header';
import BottomNav from '../BottomNav/BottomNav';
import styles from './styles';

class NavDrawer extends Component {
  toggleNavAction = () => {
    const { handleDrawerToggle, mobileOpen } = this.props;
    const width = window.innerWidth;

    if (width < 1280 || mobileOpen) {
      handleDrawerToggle();
    }
  }

  render() {
    const {
      classes,
      theme,
      container,
      children,
      handleDrawerToggle,
      mobileOpen,
      robotsQA,
      manifest
    } = this.props;

    const QAIcon = () => {
      const qaSize = robotsQA.length;

      if (qaSize) {
        return(
          <Badge color="error" badgeContent={qaSize}>
            <LibraryBooks />
          </Badge>
        )
      }

      return <LibraryBooks />;
    }

    const NewShipmentIcon = () => {
      const manifestSize = manifest.length;

      if (manifestSize) {
        return(
          <Badge color="error" badgeContent={manifestSize}>
            <LocalShipping />
          </Badge>
        )
      }

      return <LocalShipping />;
    }

    const drawer = (
      <div>
        <div className={classes.toolbar} />
        <Divider />
        <List>
          <Link to="/" onClick={this.toggleNavAction}>
            <ListItem button>
              <ListItemIcon><Android /></ListItemIcon>
              <ListItemText primary="Robots" />
            </ListItem>
          </Link>
        </List>
        <Divider />
        <List>
          <Link to="/qa" onClick={this.toggleNavAction}>
            <ListItem button>
              <ListItemIcon><QAIcon /></ListItemIcon>
              <ListItemText primary="Quality Assurance" />
            </ListItem>
          </Link>
        </List>
        <Divider />
        <List>
          <Link to="/shipments" onClick={this.toggleNavAction}>
            <ListItem button>
              <ListItemIcon><LocalShipping /></ListItemIcon>
              <ListItemText primary="Shipments" />
            </ListItem>
          </Link>
          <Link to="/shipments/new" onClick={this.toggleNavAction}>
            <ListItem button>
              <ListItemIcon><NewShipmentIcon /></ListItemIcon>
              <ListItemText primary="New Shipment" />
            </ListItem>
          </Link>
        </List>
      </div>
    );

    return (
      <div className={classes.root}>
        <CssBaseline />
        <Header handleOnClick={handleDrawerToggle} />
        <nav className={classes.drawer}>
          <Hidden lgUp implementation="css">
            <Drawer
              container={container}
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper,
              }}
              ModalProps={{
                keepMounted: true
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden mdDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
        <BottomNav />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {children}
        </main>
      </div>
    );
  }
}

NavDrawer.defaultProps = {
  robotsQA: [],
  mobileOpen: false
}

NavDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  children: PropTypes.element.isRequired,
  robotsQA: PropTypes.array,
  handleDrawerToggle: PropTypes.func.isRequired,
  mobileOpen: PropTypes.bool
}

const mapStateToProps = ({ mobileOpen, robotsQA, manifest }) => ({
  mobileOpen, robotsQA, manifest
});

const withstyle = withStyles(styles, { withTheme: true })(NavDrawer);

export default connect(mapStateToProps, {
  handleDrawerToggle: toggleNav
})(withstyle);
