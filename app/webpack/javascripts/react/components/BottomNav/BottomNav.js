import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import LocalShipping from '@material-ui/icons/LocalShipping';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import Android from '@material-ui/icons/Android';

import styles from './styles';

const BottomNav = ({ classes }) => {
  return(
    <AppBar position="fixed" className={classes.bottomBar} color="default">
      <Toolbar className={classes.bottomToolBar}>
        <Link to="/qa">
          <IconButton>
            <LibraryBooks />
          </IconButton>
        </Link>
        <Link to="/">
          <IconButton>
            <Android />
          </IconButton>
        </Link>
        <Link to="/shipments">
          <IconButton>
            <LocalShipping />
          </IconButton>
        </Link>
      </Toolbar>
    </AppBar>
  )
}

export default withStyles(styles)(BottomNav);
