const drawerWidth = 300;

export default theme => ({
  bottomBar: {
    top: 'auto',
    bottom: 0,
    width: '100%',
    height: 'auto',
    position: 'fixed',
    display: 'flex',
    borderTop: '1px solid #f0f0f0',
    background: '#fff',
    [theme.breakpoints.up('lg')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    }
  },
  bottomToolBar: {
    justifyContent: 'space-evenly',
    minHeight: 45,
    '& svg': {
      fontSize: 20
    }
  }
});
