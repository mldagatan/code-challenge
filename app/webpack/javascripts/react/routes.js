import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Homepage from './containers/Homepage/Homepage';
import QualityAssurance from './containers/QA/QualityAssurance';
import Shipments from './containers/Shipments/Shipments';
import Shipment from './containers/Shipments/Shipment/Shipment';
import NewShipment from './containers/Shipments/NewShipment/NewShipment';

export default () => (
  <Router>
    <Switch>
      <Route path="/shipments/new" component={NewShipment} />
      <Route path="/shipments/:id" component={Shipment} />
      <Route path="/shipments" component={Shipments} />
      <Route path="/qa" component={QualityAssurance} />
      <Route path="/" component={Homepage} />
    </Switch>
  </Router>
)
