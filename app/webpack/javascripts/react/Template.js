import React from 'react';
import PropTypes from 'prop-types';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import NavDrawer from './components/NavDrawer/NavDrawer';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#111'
    }
  },
  typography: {
    htmlFontSize: 17,
  },
  overrides: {
    MuiListItem: {
      root: {
        paddingBottom: 8,
        paddingTop: 8
      }
    },
    MuiSvgIcon: {
      root: {
        fontSize: 21
      }
    }
  }
});

const TemplateProps = {
  children: PropTypes.element.isRequired
}

export default ({ children }: TemplateProps) => (
  <MuiThemeProvider theme={theme}>
    <NavDrawer>
      {children}
    </NavDrawer>
  </MuiThemeProvider>
);
