export default () => ([
  {
    id: 1,
    name: "She-Hulk",
    configuration: {
    colour: "plum",
    hasTracks: false,
    hasWheels: true,
    hasSentience: false,
    numberOfRotors: 5
    },
    statuses: [
    "loose screws",
    "paint scratched",
    "on fire",
    "rusty"
    ],
    created_at: "2018-11-08T09:20:41.082Z",
    updated_at: "2018-11-08T09:20:41.082Z",
    report: {
    id: 1,
    robot_id: 1,
    conclusion: "recycle",
    created_at: "2018-11-08T11:16:33.728Z",
    updated_at: "2018-11-08T11:16:33.728Z"
    }
    },
    {
    id: 2,
    name: "Doctor Hulk",
    configuration: {
    colour: "lime",
    hasTracks: false,
    hasWheels: false,
    hasSentience: true,
    numberOfRotors: 5
    },
    statuses: [],
    created_at: "2018-11-08T09:20:41.091Z",
    updated_at: "2018-11-08T09:20:41.091Z"
    },
    {
    id: 3,
    name: "Magnificent Martian Manhunter",
    configuration: {
    colour: "fuchsia",
    hasTracks: true,
    hasWheels: false,
    hasSentience: true,
    numberOfRotors: 3
    },
    statuses: [
    "on fire",
    "rusty",
    "paint scratched"
    ],
    created_at: "2018-11-08T09:20:41.099Z",
    updated_at: "2018-11-08T09:20:41.099Z"
  }
])
