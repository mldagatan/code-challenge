import {
  ADD_ROBOT_TO_QA,
  REMOVE_ROBOT_FROM_QA,
  CLEAR_QA
} from '../actions/actionTypes';

export default (state = [], action) => {
  switch(action.type) {
    case ADD_ROBOT_TO_QA:
      return [...state, action.payload]
    case REMOVE_ROBOT_FROM_QA:
      return state.filter(id => id !== action.payload);
    case CLEAR_QA:
    return []
    default:
      return state;
  }
}
