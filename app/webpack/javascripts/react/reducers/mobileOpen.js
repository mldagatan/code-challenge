import { TOGGLE_NAV } from '../actions/actionTypes';

export default (state = false, action) => {
  if (action.type === TOGGLE_NAV) {
    return !state;
  }

  return state;
}
