import _ from 'lodash';

import {
  ROBOTS_FETCH,
  EXTINGUISH_ROBOT,
  PROCESS_QUALITY_ASSURANCE
} from '../actions/actionTypes';

export default (state = {}, action) => {
  switch(action.type) {
    case ROBOTS_FETCH:
      return _.mapKeys(action.payload.data, 'id');
    case EXTINGUISH_ROBOT:
      return {
        ...state, [action.payload.data.robot.id]: action.payload.data.robot
      }
    case PROCESS_QUALITY_ASSURANCE:
      return { ...state, ...(_.mapKeys(action.payload.data, 'id')) }
    default:
      return state;
  }
}
