import {
  ADD_TO_MANIFEST,
  REMOVE_FROM_MANIFEST,
  CLEAR_MANIFEST
} from '../actions/actionTypes';

export default(state = [], action) => {
  switch(action.type) {
    case ADD_TO_MANIFEST:
      return [...state, action.payload].sort();
    case REMOVE_FROM_MANIFEST:
    return state.filter(id => id !== action.payload).sort();
    case CLEAR_MANIFEST:
      return [];
    default:
      return state;
  }
}
