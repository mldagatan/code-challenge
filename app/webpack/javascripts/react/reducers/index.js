import { combineReducers } from 'redux';

import robotsReducer from './robots';
import robotsQAReducer from './robotsQA';
import mobileOpenReducer from './mobileOpen';
import shipmentsReducer from './shipments';
import manifestReducer from './manifest';

export default combineReducers({
  robots: robotsReducer,
  robotsQA: robotsQAReducer,
  mobileOpen: mobileOpenReducer,
  shipments: shipmentsReducer,
  manifest: manifestReducer
});
