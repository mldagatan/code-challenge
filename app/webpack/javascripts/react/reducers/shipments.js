import _ from 'lodash';

import { SHIPMENTS_FETCH, CREATE_SHIPMENT } from '../actions/actionTypes';

export default (state = {}, action) => {
  switch(action.type) {
    case CREATE_SHIPMENT:
      return { ...state, [action.payload.data.id]: action.payload.data }
    case SHIPMENTS_FETCH:
      return _.mapKeys(action.payload.data, 'id')
    default:
      return state;
  }
}
