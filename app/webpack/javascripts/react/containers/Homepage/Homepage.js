import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import RobotsList from "../RobotsList/RobotsList";
import Template from "../../Template";
import styles from './styles';

import toTop from "../../components/ToTop";

const filters = [
  'all',
  'needs qa',
  'finished qa',
  'shipment ready',
  'in shipment'
]

class Homepage extends Component {
  state = {
    filterBy: 'all'
  }

  constructor() {
    super();

    toTop();
  }

  setFilter = (filterBy) => {
    this.setState({ filterBy });
  }

  filteredRobots() {
    const { robots } = this.props;
    const { filterBy } = this.state;

    switch(filterBy) {
      case 'needs qa':
      return _.pickBy(robots, robot => {
        const robotKeys = _.keys(robot);
        return !robotKeys.includes('report');
      });
      case 'finished qa':
        return _.pickBy(robots, robot => {
          const robotKeys = _.keys(robot);
          return robotKeys.includes('report');
        });
      case 'shipment ready':
        return _.pickBy(robots, robot=> {
          const robotKeys = _.keys(robot);
          const hasReport = robotKeys.includes('report');
          const noShipInfo = !robotKeys.includes('shipping_information');

          let notRecycle = false;
          if (hasReport) {
            const { conclusion } = robot.report;
            notRecycle = ['passed', 'factory second'].includes(conclusion);
          }

          return hasReport && noShipInfo && notRecycle
        });
      case 'in shipment':
      return _.pickBy(robots, robot => {
        const robotKeys = _.keys(robot);
        const hasReport = robotKeys.includes('report');
        const hasShipInfo = robotKeys.includes('shipping_information');

        return hasReport && hasShipInfo;
      });
      default:
        return robots;
    }
  }

  renderFilters() {
    const { filterBy } = this.state;
    return(
      filters.map(filter => (
        <Button
          disabled={filterBy === filter}
          onClick={() => this.setFilter(filter)}
          key={filter}
        >
          {filter.toUpperCase()}
        </Button>
      ))
    )
  }

  renderRobotsList() {
    const list = _.values(this.filteredRobots());
    if (list.length) {
      return(
        <RobotsList robots={Object.values(this.filteredRobots())} />

      )
    }

    return(
      <div>No robots in this list.</div>
    )
  }

  render() {
    const { classes } = this.props;

    return(
      <Template>
        <div>
          <h2>Robots</h2>
          <Paper className={classes.headPaper}>
            <small>FILTER BY</small>
            <div className={classes.filtersContainer}>
              <div className={classes.filters}>
                {this.renderFilters()}
              </div>
            </div>
          </Paper>
          {this.renderRobotsList()}
        </div>
      </Template>
    )
  }
}

Homepage.propTypes = {
  robots: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired
}

const mapStateToProps = state => ({ robots: state.robots });
const withStyle = withStyles(styles)(Homepage);

export default connect(mapStateToProps)(withStyle);
