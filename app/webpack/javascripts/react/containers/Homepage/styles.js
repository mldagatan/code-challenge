export default () => ({
  headPaper: {
    padding: '1rem'
  },
  filtersContainer: {
    width: '100%',
    overflowX: 'scroll'
  },
  filters: {
    width: 'max-content'
  }
})
