import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import Template from '../../../Template';
import RobotsList from '../../RobotsList/RobotsList';
import styles from './styles';
import toTop from '../../../components/ToTop';

import { createShipment, fetchRobots, clearManifest } from '../../../actions';

class NewShipment extends Component {
  constructor() {
    super();

    toTop();
  }

  handleClickCreate = () => {
    const { manifest, create, fetch, history, clear } = this.props;

    create(manifest);
    fetch();
    clear();
    history.push('/shipments');
  }

  renderActions() {
    const { classes, manifest } = this.props;

    const createButton = () => {
      if (manifest.length) {
        return(
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleClickCreate}
          >
            Create Shipment
          </Button>
        );
      }

      return(
        <p>
          <small>Atleast 1 robot needs to be in the shipping manifest.</small>
        </p>
      );
    }

    return(
      <Paper className={classes.paper}>
        {createButton()}
      </Paper>
    )
  }

  renderFactorySeconds() {
    const { factorySeconds } = this.props;

    if (!_.keys(factorySeconds).length) {
      return <div>No Robots considered as Factory Second.</div>;
    }

    return(
      <div>
        <RobotsList robots={Object.values(factorySeconds)} />
      </div>
    );
  }

  renderPassedQA() {
    const { passed } = this.props;

    if (!_.keys(passed).length) {
      return <div>No Robots passed QA.</div>;
    }

    return(
      <div>
        <RobotsList robots={Object.values(passed)} />
      </div>
    );
  }

  renderManifest() {
    const { inManifest } = this.props;

    if (!_.keys(inManifest).length) {
      return <div>No Robots in shipping manifest</div>;
    }

    return(
      <div>
        <RobotsList robots={Object.values(inManifest)} />
      </div>
    );
  }

  renderTitle(title) {
    return <h3>{title}</h3>;
  }

  render() {
    const { manifest } = this.props;
    const manifestTitle = `Shipping Manifest (${manifest.length})`;

    return(
      <Template>
        <div>
          <h2>New Shipment</h2>
          {this.renderActions()}
          {this.renderTitle(manifestTitle)}
          {this.renderManifest()}
          {this.renderTitle('Passed Quality Assurance')}
          {this.renderPassedQA()}
          {this.renderTitle('Factory Seconds')}
          {this.renderFactorySeconds()}
        </div>
      </Template>
    )
  }
}

const mapStateToProps = ({ robots, manifest }) => {
  let factorySeconds;
  let passed;
  let inManifest;

  if (robots) {
    const robotsToShip = _.pickBy(robots, r => {
      const robotKeys = _.keys(r);
      const hasReport = robotKeys.includes('report');
      const noShipInfo = !robotKeys.includes('shipping_information');
      const notInManifest = !manifest.includes(r.id);

      return hasReport && noShipInfo && notInManifest
    });

    factorySeconds = _.pickBy(robotsToShip, ({ report }) => (
      report.conclusion === 'factory second'
    ));

    passed = _.pickBy(robotsToShip, ({ report }) => (
      report.conclusion === 'passed'
    ));

    inManifest = _.pick(robots, manifest);
  }

  return({
    inManifest,
    passed,
    factorySeconds,
    manifest
  })
};

NewShipment.defaultProps = {
  factorySeconds: {},
  passed: {},
  inManifest: {},
  manifest: []
}

NewShipment.propTypes = {
  factorySeconds: PropTypes.object,
  passed: PropTypes.object,
  inManifest: PropTypes.object,
  manifest: PropTypes.array,
  classes: PropTypes.object.isRequired,
  create: PropTypes.func.isRequired,
  fetch: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  clear: PropTypes.func.isRequired
}

const withStyle = withStyles(styles)(NewShipment);

export default connect(mapStateToProps, {
  create: createShipment,
  fetch: fetchRobots,
  clear: clearManifest
})(withStyle);
