import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';

import styles from './styles';
import Template from '../../Template';
import ShipmentLine from './ShipmentLine';
import toTop from "../../components/ToTop";

class Shipments extends Component {
  constructor() {
    super();

    toTop();
  }

  shipmentActions() {
    const { classes } = this.props;

    return(
      <Paper className={classes.paper}>
        <Link to="/shipments/new">
          <Button variant="contained" color="primary">
            New Shipment
          </Button>
        </Link>
      </Paper>
    )
  }

  renderShipments() {
    const { shipments, history } = this.props;

    return(
      _.map(shipments, shipment => (
        <ShipmentLine
          shipment={shipment}
          key={shipment.uuid}
          history={history}
        />
      ))
    )
  }

  render() {
    const { classes } = this.props;
    return(
      <Template>
        <div>
          <h2>Shipments</h2>
          {this.shipmentActions()}
          <Paper className={classes.tablePaper}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell numeric className={classes.tc}>
                    ID
                  </TableCell>
                  <TableCell numeric className={classes.tc}>
                    UUID
                  </TableCell>
                  <TableCell className={classes.tc}>Creation Date</TableCell>
                  <TableCell className={classes.tc}>Shipment Date</TableCell>
                  <TableCell numeric className={classes.tc}>
                    # of Robots
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.renderShipments()}
              </TableBody>
            </Table>
          </Paper>
        </div>
      </Template>
    );
  }
}

Shipments.propTypes = {
  classes: PropTypes.object.isRequired,
  shipments: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}

const mapStateToProps = ({ shipments, robots }) => ({ shipments, robots });
const componentWithStyles = withStyles(styles)(Shipments);

export default connect(mapStateToProps)(componentWithStyles);
