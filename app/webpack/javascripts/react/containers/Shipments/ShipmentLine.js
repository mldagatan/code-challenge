import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import styles from './styles';

class ShipmentLine extends Component {
  constructor() {
    super();
    this.handleOnClick = this.handleOnClick.bind(this);
  }

  handleOnClick = () => {
    const { shipment, history } = this.props;
    history.push(`/shipments/${shipment.id}`);
  }

  render() {
    const { shipment, classes } = this.props;

    return(
      <TableRow
        id={shipment.id}
        onClick={this.handleOnClick}
        className={classes.shipmentRow}
      >
        <TableCell numeric className={classes.tc}>
          {shipment.id}
        </TableCell>
        <TableCell numeric className={classes.tc}>
          {shipment.uuid}
        </TableCell>
        <TableCell className={classes.tc}>
          {shipment.created_at}
        </TableCell>
        <TableCell className={classes.tc}>
          {shipment.shipment_date}
        </TableCell>
        <TableCell numeric className={classes.tc}>
          {shipment.robot_ids.length}
        </TableCell>
      </TableRow>
    )
  }
}

ShipmentLine.propTypes = {
  shipment: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}

export default withStyles(styles)(ShipmentLine);
