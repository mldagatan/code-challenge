export default () => ({
  paper: {
    padding: '0.5rem 1rem',
    marginBottom: '1rem',
  },
  tablePaper: {
    overflowX: 'scroll',
    width: '100%'
  },
  table: {
    minWidth: 600
  },
  tc: {
    padding: '1rem 0.5rem'
  },
  shipmentRow: {
    transition: 'all 100ms ease',
    '&:hover': {
      backgroundColor: '#f0f0f0',
      cursor: 'pointer'
    }
  }
});
