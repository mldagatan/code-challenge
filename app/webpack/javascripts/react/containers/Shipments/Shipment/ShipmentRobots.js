import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const ShipmentRobots = ({ robots }) => {
  if (!robots) {
    return(
      <div>Loading Robots</div>
    )
  }

  return(
    <List>
      {
        _.map(robots, robot => {
          return(
            <ListItem key={robot.id}>
              <ListItemText>{robot.name}</ListItemText>
            </ListItem>
          )
        })
      }
    </List>
  )
};

ShipmentRobots.propTypes = {
  robots: PropTypes.object.isRequired
}

export default ShipmentRobots;
