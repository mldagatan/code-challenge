import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect }from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import Template from '../../../Template';
import styles from './styles';
import RobotsList from '../../RobotsList/RobotsList';
import toTop from "../../../components/ToTop";

class Shipment extends Component {
  constructor() {
    super();

    toTop();
  }

  renderShipment() {
    const {
      shipment: {
        uuid,
        id,
        shipment_date,
        created_at,
        robot_ids
      },
      classes,
      robots,
    } = this.props;

    return(
      <Paper className={classes.paper}>
        <h2>
          Shipment
        </h2>
        <div className={classes.infoContainer}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>UUID</TableCell>
                <TableCell>Created At</TableCell>
                <TableCell>Shipment Date</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>{id}</TableCell>
                <TableCell>{uuid}</TableCell>
                <TableCell>{created_at}</TableCell>
                <TableCell>{shipment_date}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </div>
        <h4>
          Robots
          {robot_ids ? ` (${robot_ids.length})` : ''}
        </h4>
        <RobotsList robots={Object.values(robots)} />
      </Paper>
    );
  }

  render() {
    return(
      <Template>
        <div>
          {this.renderShipment()}
        </div>
      </Template>
    )
  }
}

Shipment.defaultProps = {
  shipment: {},
  robots: {}
}

Shipment.propTypes = {
  shipment: PropTypes.object,
  robots: PropTypes.object,
  classes: PropTypes.object.isRequired
}

const mapStateToProps = ({ shipments, robots }, ownProps) => {
  const { id } = ownProps.match.params;
  const shipment = shipments[id];
  let shipmentRobots;
  if (shipment) {
    const { robot_ids } = shipment;

    if (_.keys(robots).length) {
      shipmentRobots = _.mapKeys(_.at(robots, robot_ids), 'id');
    }
  }

  return { shipment, robots: shipmentRobots }
};

const withStyle = withStyles(styles)(Shipment);

export default connect(mapStateToProps)(withStyle);
