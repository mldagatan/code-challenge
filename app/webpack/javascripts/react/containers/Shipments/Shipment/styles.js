export default () => ({
  paper: {
    padding: "1rem",
  },
  infoContainer: {
    overflowX: "scroll"
  }
});
