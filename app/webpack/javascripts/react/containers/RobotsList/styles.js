const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflow: 'hidden'
  },
  tableCell: {
    padding: '0.3rem 0.5rem',
    '& button': {
      width: 'max-content',
      padding: '5px 10px',
      minHeight: 30,
      height: 'auto',
    },
    '& svg': {
      fontSize: 16
    }
  },
  list: {
    paddingInlineStart: '30px',
    margin: 0
  },
  tableContainer: {
    overflowX: 'scroll',
    width: 'inherit'
  }
});

export default styles;
