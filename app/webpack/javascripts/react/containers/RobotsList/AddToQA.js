import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import { addRobotToQA as addRobotToQAAction } from '../../actions';

class AddToQA extends Component {
  constructor() {
    super();

    this.handleOnClick = this.handleOnClick.bind(this);
  }

  handleOnClick() {
    const { addRobotsToQA, robot: { id } } = this.props;

    addRobotsToQA(id);
  }

  render() {
    return(
      <Button
        variant="outlined"
        color="primary"
        onClick={this.handleOnClick}
      >
        Add to QA
      </Button>
    )
  }
};

AddToQA.propTypes = {
  robot: PropTypes.object.isRequired,
  addRobotsToQA: PropTypes.func.isRequired,
}

const mapDispatchToProps = dispatch => ({
  addRobotsToQA: id => dispatch(addRobotToQAAction(id))
})

export default connect(null, mapDispatchToProps)(AddToQA);
