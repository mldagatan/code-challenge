import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Check from '@material-ui/icons/Check';
import Close from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';

import AddToQA from './AddToQA';
import RemoveFromQA from './RemoveFromQA';
import AddToManifest from './AddToManifest';
import RemoveFromManifest from './RemoveFromManifest';
import TableNavigation from './TableNavigation/TableNavigation';

import styles from './styles';
import toTop from "../../components/ToTop";

class RobotsList extends Component {
  state = {
    page: 1,
    itemsPerPage: 50,
  }

  componentWillReceiveProps(nextProps) {
    const { robots } = this.props;
    const newRobots = nextProps.robots;

    if (_.difference(robots, newRobots).length) {
      this.setState({ page: 1 });
    }
  }

  prevPage = () => {
    const { page } = this.state;

    this.setState({ page: page - 1 });
    toTop();
  }

  nextPage = () => {
    const { page } = this.state;

    this.setState({ page: page + 1 });
    toTop();
  }

  setPage = (page) => {
    this.setState({ page });
    toTop();
  }

  setItemsPerPage = (items) => {
    this.setState({ itemsPerPage: items });
    toTop();
  }

  renderReport(robot) {
    const { robotsQA } = this.props;

    if(robot.report) {
      return robot.report.conclusion
    }

    if (robotsQA.includes(robot.id)) {
      return <RemoveFromQA robot={robot} />
    }

    return (
      <AddToQA robot={robot} />
    );
  }

  renderShippingInformation(robot) {
    const { shipping_information, report } = robot;
    const { manifest } = this.props;

    if (shipping_information) {
      const { shipment_id } = shipping_information;
      const url = `/shipments/${shipment_id}`;
      return (
        <Link to={url}>
          Shipment
          {shipment_id}
        </Link>
      );
    }

    const isNotRecycle = report ? report.conclusion !== 'recycle' : false;
    const args = report && isNotRecycle &&!shipping_information;
    const inManifest = manifest.includes(robot.id);

    if (args && !inManifest) {
      return <AddToManifest robot={robot} />;
    }

    if (args && inManifest) {
      return <RemoveFromManifest robot={robot} />;
    }

    return 'n/a';
  }

  renderRobots() {
    const { robots, classes } = this.props;
    const { page, itemsPerPage } = this.state;

    const begin = ((page * itemsPerPage) - itemsPerPage);
    const end = (page * itemsPerPage);
    const robotsToRender = robots.slice(begin, end);

    const renderBool = (bool) => {
      if (bool) {
        return <Check />
      }
      return <Close />
    }

    const reportClasses = `${classes.tableCell} report`;

    return(
      robotsToRender.map(robot => (
        <TableRow key={robot.id} className="robot-row">
          <TableCell numeric className={classes.tableCell}>
            {robot.id}
          </TableCell>
          <TableCell className={classes.tableCell}>{robot.name}</TableCell>
          <TableCell className={reportClasses}>
            {this.renderReport(robot)}
          </TableCell>
          <TableCell>
            {this.renderShippingInformation(robot)}
          </TableCell>
          <TableCell className={classes.tableCell}>
            {robot.configuration.colour}
          </TableCell>
          <TableCell className={classes.tableCell}>
            {renderBool(robot.configuration.hasTracks)}
          </TableCell>
          <TableCell className={classes.tableCell}>
            {renderBool(robot.configuration.hasWheels)}
          </TableCell>
          <TableCell className={classes.tableCell}>
            {renderBool(robot.configuration.hasSentience)}
          </TableCell>
          <TableCell
            numeric
            className={classes.tableCell}
          >
            {robot.configuration.numberOfRotors}
          </TableCell>
          <TableCell className={classes.tableCell}>
            <ul className={classes.list}>
              {robot.statuses.map(status => (<li key={status}>{status}</li>))}
            </ul>
          </TableCell>
        </TableRow>
      ))
    )
  }

  renderNav() {
    const { page, itemsPerPage } = this.state;
    const { robots } = this.props;
    const totalItems = _.keys(robots).length;

    return(
      <TableNavigation
        page={page}
        itemsPerPage={itemsPerPage}
        nextPage={this.nextPage}
        prevPage={this.prevPage}
        setPage={this.setPage}
        setItemsPerPage={this.setItemsPerPage}
        totalItems={totalItems}
      />
    )
  }

  render() {
    const { classes, robots } = this.props;

    if (!_.keys(robots).length) {
      return(
        <div>The robots are being prepped. Please wait.</div>
      );
    }

    return(
      <Paper className={classes.root}>
        {this.renderNav()}
        <div className={classes.tableContainer}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell numeric className={classes.tableCell}>ID</TableCell>
                <TableCell className={classes.tableCell}>Name</TableCell>
                <TableCell className={classes.tableCell}>QA Report</TableCell>
                <TableCell className={classes.tableCell}>Shipment</TableCell>
                <TableCell className={classes.tableCell}>Colour</TableCell>
                <TableCell className={classes.tableCell}>Tracks</TableCell>
                <TableCell className={classes.tableCell}>Wheels</TableCell>
                <TableCell className={classes.tableCell}>Sentience</TableCell>
                <TableCell numeric className={classes.tableCell}>
                  Rotors
                </TableCell>
                <TableCell className={classes.tableCell}>Statuses</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.renderRobots()}
            </TableBody>
          </Table>
        </div>
        {this.renderNav()}
      </Paper>
    )
  }
}

RobotsList.defaultProps = {
  robotsQA: [],
  manifest: []
}

RobotsList.propTypes = {
  robots: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  robotsQA: PropTypes.array,
  manifest: PropTypes.array
}

const robotsWithStyles = withStyles(styles)(RobotsList)
const mapStateToProps = ({ robotsQA, manifest }) => ({ robotsQA, manifest });

export default connect(mapStateToProps)(robotsWithStyles);
