import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import IconButton from '@material-ui/core/IconButton';
import LocalShipping from '@material-ui/icons/LocalShipping';
import Remove from '@material-ui/icons/Remove';

import { removeFromManifest } from '../../actions';

class RemoveFromManifest extends Component {
  handleOnClick = () => {
    const { remove, robot: {id} } = this.props;
    remove(id);
  }

  render() {
    return(
      <IconButton onClick={this.handleOnClick} color="secondary">
        <Remove />
        <LocalShipping />
      </IconButton>
    )
  }
}

RemoveFromManifest.propTypes = {
  robot: PropTypes.object.isRequired,
  remove: PropTypes.func.isRequired
}

export default connect(null, {
  remove: removeFromManifest
})(RemoveFromManifest);
