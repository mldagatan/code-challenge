import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import { removeRobotFromQA as removeRobotFromQAAction } from '../../actions';

class RemoveFromQA extends Component {
  constructor() {
    super();
    this.handleOnClick = this.handleOnClick.bind(this);
  }

  handleOnClick() {
    const { removeRobotFromQA, robot: { id } } = this.props;

    removeRobotFromQA(id);
  }

  render() {
    return(
      <Button
        variant="outlined"
        color="secondary"
        onClick={this.handleOnClick}
      >
        Remove from QA
      </Button>
    )
  }
}

RemoveFromQA.propTypes = {
  robot: PropTypes.object.isRequired,
  removeRobotFromQA: PropTypes.func.isRequired
}

const mapDispatchToProps = dispatch => ({
  removeRobotFromQA: id => dispatch(removeRobotFromQAAction(id))
})

export default connect(null, mapDispatchToProps)(RemoveFromQA);
