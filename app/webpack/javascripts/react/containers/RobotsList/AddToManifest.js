import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import IconButton from '@material-ui/core/IconButton';
import LocalShipping from '@material-ui/icons/LocalShipping';
import Add from '@material-ui/icons/Add';

import { addToManifest } from '../../actions';

class AddToManifest extends Component {
  handleOnClick = () => {
    const { add, robot: {id} } = this.props;
    add(id);
  }

  render() {
    return(
      <IconButton onClick={this.handleOnClick} color="primary">
        <Add />
        <LocalShipping />
      </IconButton>
    )
  }
}

AddToManifest.propTypes = {
  robot: PropTypes.object.isRequired,
  add: PropTypes.func.isRequired

}

export default connect(null, { add: addToManifest })(AddToManifest);
