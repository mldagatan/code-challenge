import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import NavigateNext from '@material-ui/icons/NavigateNext';
import NavigateBefore from '@material-ui/icons/NavigateBefore';

import styles from './styles';

class TableNavigation extends Component {
  totalPages() {
    const { itemsPerPage, totalItems } = this.props;
    if (totalItems && itemsPerPage) {
      return Math.ceil(totalItems / itemsPerPage);
    }

    return 1
  }

  renderPrevButton() {
    const { page, prevPage, classes } = this.props;
    const isFirstPage = page === 1;

    return (
      <IconButton
        disabled={isFirstPage}
        onClick={prevPage}
        className={classes.pageBtn}
      >
        <NavigateBefore />
      </IconButton>
    );
  }

  renderNextButton() {
    const { page, nextPage, classes } = this.props;
    const isLastPage = page >= this.totalPages();

    return(
      <IconButton
        disabled={isLastPage}
        onClick={nextPage}
        className={classes.pageBtn}
      >
        <NavigateNext />
      </IconButton>
    );
  }

  renderPages() {
    const { page, setPage, classes } = this.props;
    const arr = _.times(this.totalPages(), Number);

    return(
      arr.map(i => {
        const p = i + 1;
        return(
          <Button
            key={p}
            className="page-btn"
            disabled={page === p}
            onClick={() => setPage(p)}
            className={classes.page}
          >
            {p}
          </Button>
        )
      })
    )
  }

  render() {
    const { classes } = this.props;

    return(
      <div className={classes.root}>
        {this.renderPrevButton()}
        <div className={classes.pagesContainer}>
          <div className={classes.pages}>
            {this.renderPages()}
          </div>
        </div>
        {this.renderNextButton()}
      </div>
    )
  }
}

TableNavigation.defaultProps = {
  page: 1,
  totalItems: 0,
  itemsPerPage: 0
}

TableNavigation.propTypes = {
  page: PropTypes.number,
  totalItems: PropTypes.number,
  itemsPerPage: PropTypes.number,
  prevPage: PropTypes.func.isRequired,
  nextPage: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
  setItemsPerPage: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TableNavigation);
