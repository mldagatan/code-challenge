export default theme => ({
  root: {
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.up('md')]: {
      justifyContent: 'center'
    },
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'space-between'
    },
  },
  pagesContainer: {
    overflowX: 'scroll',
    maxWidth: 'calc(100% - 2rem)',
    [theme.breakpoints.down('sm')]: {
      width: 'calc(100% - 2rem)'
    },
  },
  pages: {
    width: 'max-content'
  },
  page: {
    minWidth: 40
  },
  pageBtn: {
    width: '1rem'
  }
});
