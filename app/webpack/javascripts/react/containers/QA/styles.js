export default (theme) => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  actionGroup: {
    marginTop: '1rem',
    '& button': {
      marginRight: '0.25rem'
    }
  }
});
