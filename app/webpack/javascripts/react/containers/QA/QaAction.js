import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

const QaAction = ({
  classes,
  robotsCount,
  processQA,
  finishedQA,
  clearQAList
}) => {
  const clearQABtn = () => {
    if (robotsCount > 0) {
      return(
        <Button onClick={clearQAList} variant="outlined" color="secondary">
          Clear QA List
        </Button>
      )
    }

    return('');
  }

  const message = () => {
    if (robotsCount < 10) {
      return(
        'Robots needs to be alteast 10 and less than 1000 before QA can proceed.'
      );
    }

    return(
      <div>
        <Button color="secondary" variant="contained" onClick={processQA}>
          Proceed with QA
        </Button>
        <p>
          <small>
            You have atleast 10 robots, you may proceed with the Quality
            Assurance Process.
          </small>
        </p>
      </div>
    )
  }
  if (finishedQA) {
    return(
      <Paper className={classes.root}>
        <h3>Successfully finished QA!</h3>
        <p>The results of the QA can be seen below.</p>
        <p>
          To ship them to shops, please fill up the shipping manifest and
          create a shipment by clicking the button below.
        </p>
        <p>
          <small>
            Clicking on this button will automatically clear out the Quality
            Assurance list.
          </small>
        </p>
        <Link to="/shipments/new" onClick={clearQAList}>
          <Button color="primary" variant="contained">
            Create Shipment
          </Button>
        </Link>
        {clearQABtn()}
      </Paper>
    )
  }

  return(
    <Paper className={classes.root}>
      {message()}
      <div className={classes.actionGroup}>
        <Link to="/">
          <Button color="primary" variant="outlined">
            Add more robots.
          </Button>
        </Link>
        {clearQABtn()}
      </div>
    </Paper>
  );
};

QaAction.defaultProps = {
  robotsCount: 0
}

QaAction.propTypes = {
  classes: PropTypes.object.isRequired,
  robotsCount: PropTypes.number,
  processQA: PropTypes.func.isRequired,
  finishedQA: PropTypes.bool.isRequired,
  clearQAList: PropTypes.func.isRequired
}

export default withStyles(styles)(QaAction);
