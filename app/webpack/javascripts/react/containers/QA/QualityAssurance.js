import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

import Template from '../../Template';
import RobotsList from '../RobotsList/RobotsList';
import QaAction from './QaAction';

import Extinguish from './Extinguish/Extinguish';
import { processQA, clearQA } from '../../actions';
import toTop from "../../components/ToTop";

class QualityAssurance extends Component {
  constructor() {
    super();

    toTop();
  }

  clearQAList = () => {
    const { clear } = this.props;
    clear();
  }

  processQA = () => {
    const { sendQA } = this.props;

    const recycleRobots = [];
    const factorySeconds = [];
    const passedQA = [];

    const recycleCriteria = robot => {
      const {
        hasTracks,
        hasWheels,
        hasSentience,
        numberOfRotors,
        colour
      } = robot.configuration;

      const rotorNo = !_.inRange(numberOfRotors, 3, 8);
      const blueWRotor = numberOfRotors > 0 && colour === 'blue';
      const wheelsTracks = hasWheels && hasTracks;
      const wheelsRust = hasWheels && robot.statuses.includes('rusty');
      const aliveLoose = hasSentience && robot.statuses.includes('loose screws');
      const isOnFire = robot.statuses.includes('on fire');

      const args1 = rotorNo || blueWRotor || wheelsTracks || wheelsRust;
      const args2 = aliveLoose || isOnFire

      if (args1 || args2) {
        return true;
      }

      return false;
    }

    const isFactorySecond = robot => {
      const secondStatuses = ['rusty', 'loose screws', 'paint scratced'];
      const { statuses } = robot;

      return statuses.some(status => secondStatuses.includes(status));
    }

    _.map(this.qaList(), robot => {
      if (recycleCriteria(robot)) {
        recycleRobots.push(robot.id);
      } else if (isFactorySecond(robot)) {
        factorySeconds.push(robot.id);
      } else {
        passedQA.push(robot.id);
      }
    });

    const payload = { recycleRobots, factorySeconds, passedQA };

    sendQA(payload);
  }

  qaList() {
    const { robots, robotsQA } = this.props;

    return _.pick(robots, robotsQA);
  }

  isQaFinished() {
    let finished = true;

    if (_.keys(this.qaList()).length) {
      _.map(this.qaList(), robot => {
        const hasReport = Object.prototype.hasOwnProperty.call(robot, 'report');

        if (!hasReport) {
          finished = false;
        }
      });
    } else {
      finished = false;
    }

    return finished;
  }

  checkForOnFire() {
    const { robots, robotsQA } = this.props;
    const list = _.pick(robots, robotsQA);

    let toExtinguish = {};

    _.map(list, robot => {
      const isOnFire = robot.statuses.includes('on fire');
      const { hasSentience } = robot.configuration;

      if (isOnFire && hasSentience) {
        toExtinguish = { ...toExtinguish, [robot.id]: robot }
      }
    });

    if (_.keys(toExtinguish).length) {
      return(<Extinguish robots={toExtinguish} />);
    }

    return('');
  }

  renderQaList() {
    if (!_.keys(this.qaList()).length) {
      return(
        <p>No robots to list. Please add robots.</p>
      )
    }

    return(
      <RobotsList robots={Object.values(this.qaList())} />
    );
  }

  render() {
    return(
      <Template>
        <div>
          <h2>Quality Assurance</h2>
          <QaAction
            robotsCount={_.keys(this.qaList()).length}
            processQA={this.processQA}
            finishedQA={this.isQaFinished()}
            clearQAList={this.clearQAList}
          />
          {this.checkForOnFire()}
          {this.renderQaList()}
        </div>
      </Template>
    );
  }
}

QualityAssurance.defaultProps = {
  robots: {},
  robotsQA: []
}

QualityAssurance.propTypes = {
  robots: PropTypes.object,
  robotsQA: PropTypes.array,
  sendQA: PropTypes.func.isRequired,
  clear: PropTypes.func.isRequired
}

const mapStateToProps = ({ robots, robotsQA }) => ({ robots, robotsQA });

export default connect(mapStateToProps, {
  sendQA: processQA, clear: clearQA
})(QualityAssurance);
