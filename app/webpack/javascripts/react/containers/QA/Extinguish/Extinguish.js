import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { extinguishRobot } from '../../../actions';

class Extinguish extends Component {
  state = {
    open: false
  }

  constructor() {
    super();

    this.handleExtinguish = this.handleExtinguish.bind(this);
  }

  componentDidMount() {
    this.setState({ open: true });
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  handleExtinguish = (e) => {
    const { extinguish } = this.props;
    const id = e.currentTarget.getAttribute('robot-id');

    extinguish(id);
  }

  renderRobots() {
    const { robots } = this.props;

    return(
      _.map(robots, robot => (
        <ListItem
          button
          onClick={this.handleExtinguish}
          robot-id={robot.id}
          key={robot.id}
        >
          <ListItemText>
            {robot.name}
          </ListItemText>
        </ListItem>
      ))
    )
  }

  render() {
    const { open } = this.state;
    return(
      <div>
        <Dialog
          open={open}
          onClose={this.handleClose}
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="md"
        >
          <DialogTitle>
            <i className="icon ion-md-flame" />
            ROBOTS ARE ON FIRE!!!
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              We've got to help the robots! Some of them are sentient and have
              feelings too  you know.

              Click on the robot to extinguish them!
            </DialogContentText>
            <List>
              {this.renderRobots()}
            </List>
            <DialogContentText>
              <small>
                You cannot move forward without extinguishing the robots on
                fire.
              </small>
            </DialogContentText>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

Extinguish.propTypes = {
  robots: PropTypes.object.isRequired,
  extinguish: PropTypes.func.isRequired
}

export default connect(null, { extinguish: extinguishRobot })(Extinguish);
