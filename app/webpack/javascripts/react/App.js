import React from 'react';

import Root from './Root';
import Routes from './routes';

export default () => (
  <Root>
    <Routes />
  </Root>
);
