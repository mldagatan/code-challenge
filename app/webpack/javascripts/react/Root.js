import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import reduxPromise from 'redux-promise';
import reducers from './reducers';

import { fetchRobots, fetchShipments } from './actions';

const reduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

const composeEnhancers = typeof window === 'object' && reduxDevTools ? reduxDevTools({}) : compose;

  const enhancer = composeEnhancers(
    applyMiddleware(reduxPromise)
  );

const Root = ({ initialState, children }) => {
  const store = createStore(
    reducers,
    initialState,
    enhancer
  );

  store.dispatch(fetchRobots());
  store.dispatch(fetchShipments());

  return(
    <Provider store={store}>
      {children}
    </Provider>
  );
};

Root.defaultProps = {
  initialState: {}
}

Root.propTypes = {
  children: PropTypes.element.isRequired,
  initialState: PropTypes.object
}

export default Root;
