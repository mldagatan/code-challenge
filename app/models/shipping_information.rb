class ShippingInformation < ApplicationRecord
  STATUSES = %w[pending delivered].freeze

  belongs_to :robot
  belongs_to :shipment

  validates :shipment_id, presence: true
  validates :robot_id, presence: true, uniqueness: { scope: :shipment_id }
  validates :status, presence: true, inclusion: { in: STATUSES }
end
