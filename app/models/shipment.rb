class Shipment < ApplicationRecord
  STATUSES = %w[ready enroute shipped cancelled].freeze

  validates :shipment_date, presence: true
  validates :status, presence: true, inclusion: { in: STATUSES }

  has_many :shipping_informations
  has_many :robots, through: :shipping_informations

  after_create :set_uuid

  private

  def set_uuid
    self.uuid = "ship#{Time.now.to_i}-#{format('%03d', id)}"

    save!
  end
end
