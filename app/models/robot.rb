class Robot < ApplicationRecord
  STATUSES = ['on fire', 'rusty', 'loose screws', 'paint scratched'].freeze

  has_one :shipping_information
  has_one :shipment, through: :shipping_information
  has_one :report

  validates :name, presence: true
  validate  :validate_statuses

  scope :for_qa, -> { includes(:report).where(reports: { id: nil }) }

  private

  def validate_statuses
    invalid = statuses.reject { |status| STATUSES.include?(status) }

    return unless invalid

    invalid.each do |status|
      errors.add(:statuses, "#{status} is not a valid status.")
    end
  end
end
