class Report < ApplicationRecord
  CONCLUSIONS = ['recycle', 'factory second', 'passed'].freeze

  belongs_to :robot

  validates :robot_id, presence: true
  validates :conclusion, presence: true, inclusion: { in: CONCLUSIONS }
end
