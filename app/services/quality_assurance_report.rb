class QualityAssuranceReport
  attr_accessor :for_recycle, :factory_seconds, :passed

  def initialize(payload)
    @for_recycle = Robot.where(id: payload[:recycleRobots])
    @factory_seconds = Robot.where(id: payload[:factorySeconds])
    @passed = Robot.where(id: payload[:passedQA])
  end

  def process
    process_all

    all_robots = @for_recycle + @factory_seconds + @passed

    all_robots.as_json(include: :report)
  end

  private

  def create_report(robot, conclusion)
    report = robot.report || robot.build_report
    report.conclusion = conclusion

    report.save!
  end

  def process_all
    @for_recycle.each { |robot| create_report(robot, 'recycle') }
    @factory_seconds.each { |robot| create_report(robot, 'factory second') }
    @passed.each { |robot| create_report(robot, 'passed') }
  end
end
