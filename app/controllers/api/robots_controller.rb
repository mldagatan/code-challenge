module Api
  class RobotsController < ApiController
    def index
      robots = Robot.all.limit(params[:limit])

      render json: robots.to_json(include: [
                                    :report,
                                    shipping_information: {
                                      only: %i[shipment_id status]
                                    }
                                  ])
    end

    def for_qa
      robots = Robot.for_qa.limit(params[:limit])

      render json: robots
    end

    def extinguish
      robot = Robot.find_by(id: params[:id])

      robot.statuses = robot.statuses.reject { |status| status == 'on fire' }

      if robot.save
        render json: { robot: robot }, status: 200
      else
        render json: { robot: robot, errors: robot.errors.full_messages },
               status: 422
      end
    end

    def quality_assurance
      qa = QualityAssuranceReport.new(params[:payload]).process

      render json: qa, status: 200
    end
  end
end
