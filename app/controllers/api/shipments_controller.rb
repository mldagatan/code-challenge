module Api
  class ShipmentsController < ApiController
    def index
      shipments = Shipment.all

      render json: shipments.as_json(methods: :robot_ids), status: 200
    end

    def create
      shipment = ShipmentForm.new(
        shipment_date: params[:shipment_date], robot_ids: params[:robot_ids]
      )

      if shipment.persist
        render json: shipment.shipment.as_json(methods: :robot_ids), status: 200
      else
        render json: { shipment: shipment, errors: shipment.errors },
               status: 422
      end
    end
  end
end
