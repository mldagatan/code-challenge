class ShipmentForm
  attr_reader :shipment, :errors

  def initialize(**args)
    @args = args
    @errors = []
    @shipment = nil
  end

  def persist
    shipment = Shipment.new(shipment_date: @args[:shipment_date])

    if shipment.save
      robots = Robot.where(id: @args[:robot_ids])
      shipment.robots << robots

      @shipment = shipment
      true
    else
      @errors = shipment.errors.full_messages
      false
    end
  end
end
