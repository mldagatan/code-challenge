Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'homepage#index'

  get '/qa', to: 'qa#index'
  resources :shipments, only: %i[index show new]

  namespace :api, constraints: ->(req) { req.format == 'json' } do
    resources :robots, only: :index do
      post :extinguish, on: :member
      post :quality_assurance, on: :collection
      get :for_qa, on: :collection
    end

    resources :shipments, only: [:index]

    put '/shipments/create', to: 'shipments#create'
  end
end
