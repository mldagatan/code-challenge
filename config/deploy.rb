# config valid for current version and patch releases of Capistrano
lock '~> 3.10.2'

set :application, 'code-challenge'
set :repo_url, 'git@bitbucket.org:mldagatan/code-challenge.git'

set :branch, 'develop'

set :deploy_to, '/var/www/code-challenge'

set :pty, true

set :keep_releases, 5

append :linked_files, 'config/database.yml', 'config/master.key'
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets',
       'public/system', 'node_modules'

set :rvm_type, :user
set :rvm_ruby_version, '2.4.1'

set :use_sudo, false

set :puma_rackup, -> { File.join(current_path, 'config.ru') }
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_bind, "unix://#{shared_path}/tmp/sockets/puma.sock"
set :puma_conf, "#{shared_path}/puma.rb"
set :puma_access_log, "#{shared_path}/log/puma_access.log"
set :puma_error_log, "#{shared_path}/log/puma_error.log"
set :puma_role, :app
set :puma_threads, [4, 16]
set :puma_workers, 0
set :puma_worker_timeout, nil
set :puma_init_active_record, true
set :puma_preload_app, true
