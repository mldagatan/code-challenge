
# README

This is a Ruby on Rails API with a React JS web app coding challenge requested by ZigZag Media.

You may check the staging version at, [code-challenge.marcdagatan.com](http://code-challenge.marcdagatan.com)

The website is mobile ready and I suggest you check it on mobile devices. Also, try adding to home screen for a web app experience.

## Introduction
First of, I want to apologize for the long time it took me to develop this. As I've mentioned during my interview, I only had a handful of experience with React JS. Although, I am equipped for your usual front-end frameworks such as Bootstrap and Foundation, I had to relearn React JS so I could understand well React Router, Redux, and Jest while I was studying it. A week and a half went to studying those 3 technologies.

## Technology Stack
**API**

 - Ruby 2.4.1
 - Ruby on Rails 5.2
 - Postgresql (Database)
 - Rubocop (Code Analyzer)
 - Rspec, Capybara (Testing)
 - Capistrano (Deployment)

**Web App**

- Webpack 3.12
- React JS 16.5
- Babel 6
- React Router
- Redux, with React Redux
- Material UI (Frontend Framework)
- Jest, and Enzyme (Testing) (Configured but not used due to some errors)

*I had to let go learning and using TypeScript as it was too long. I initially used Flow JS for type checking but it had problems with Jest. I dropped Flow JS and used React's internal type checking tool.*

*I also had to let go using Jest and Enzyme. I was able to configure it and was able to make initial tests for the setup and Homepage. It eventually started giving Missing Class Properties Transform errors which I was unable to solve. I gave it a day to try and solve it, but I had to move on from there.*

**Server**

Using AWS EC2
- Ubuntu Server 16.04
- Nginx
- Postgresql

## Getting Started (Local deployment)
First, make sure to have a Ruby version of 2.4.1 or higher, Node version 8.x.x or higher, and Postgresql.

Then, pull the repository.
```
git clone git@bitbucket.org:mldagatan/code-challenge.git
```

Go inside the newly pulled project
```
cd gohiso
```
Make sure you're in the develop branch

    git status

    ## You should see this ##
    # Your branch is up-to-date with 'origin/develop'.
    If not, check in to the develop branch

    git checkout develop

Fetch all the gems we need for this project by running.
```
bundle install
```

Rails 5.2 comes packaged with Webpack 3. By default it uses yarn as its package manager. If you don't have yarn yet, you may install it with the npm.
```
npm install -g yarn
```

fetch all javascript packages.
```
yarn install
```

Copy the config/database.yml.samle
```
cp config/database.yml.sampl config/database.yml
```

Below is a sample config file for the database.yml using postgresql. You may need to create a new user for this.

```
# config/database.yml

default: &default
  adapter: postgresql
  encoding: unicode
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  host: localhost
  username: codechallenge
  password: c0deCH@ll3nge
  port: 5432

development:
  <<: *default
  database: code_challenge_development

test:
  <<: *default
  database: code_challenge_test


production:
  <<: *default
  database: code_challenge_production

```    

You may need to create a new user for this project. Please do the following if you need to create a new user.

```
sudo -u postgres psql
=# create user codechallenge with password 'c0deCH@ll3nge';  
=# alter role codechallenge superuser createrole createdb replication;
```

Let's setup our database.
```
rails db:setup
```

To run Rails and Webpack dev server at the same time, we use Foreman. If you don't have it yet, please do the following.
```
gem install foreman
```

Once installed, run foreman
```
foreman start
```

Open up your browser then go to
```
http://localhost:5000

or

http://localhost:3000
```

## Running Testing
As mentioned before, we weren't able to make tests with Jest because of a problem we encountered. Although we were able to make unit tests and unit tests and request tests for the API.

You may run it by doing
```
rspec --format documentation
```

What was tested with the API:

 - Models
	 - Validation
	 - Relations
	 - Methods
- Service Objects
- Requests (API)

## API


```
GET | /api/robots.json
# Returns all robots

POST | /api/robots/{id}/extinguish.json
# Removes the status 'on fire'
# Returns a robot object

POST | /api/robots/quality_assurance.json
BODY:
{
	payload: {
		recycleRobots: [1, 2, 3],
		factorySeconds: [4, 5, 6],
		passed: [7, 8, 9]
	}
}

# Accepts a payload object with recycleRobots, factorySeconds,
# and passed as an array of robot ids.
# Returns a hash of robots with reports.

GET | /api/shipments.json
# returns all shipments.

PUT | /api/shipments/create.json
BODY:
{
	robot_ids: [1, 2, 3]
}

# Accepts robot_ids, which should be an array of robot ids.
# returns shipment object with robot_ids.
```

## File Structure

 - app
	 - controllers
		 - api
	 - webpack
		 - javascripts
			 - react (Where react is)
				 - actions
				 - components
				 - containers
				 - reducers
	 - models
	 - services (service objects)
	 - forms (form objects)
- config
- db
- spec (rails testing)
	- factories
	- models
	- requests (api requests testing)
	- services
