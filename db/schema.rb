# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_30_155358) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "reports", force: :cascade do |t|
    t.bigint "robot_id"
    t.string "conclusion", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["robot_id"], name: "index_reports_on_robot_id"
  end

  create_table "robots", force: :cascade do |t|
    t.string "name"
    t.jsonb "configuration"
    t.string "statuses", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shipments", force: :cascade do |t|
    t.string "uuid"
    t.date "shipment_date"
    t.string "status", default: "ready"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shipping_informations", force: :cascade do |t|
    t.bigint "robot_id"
    t.bigint "shipment_id"
    t.string "status", default: "pending"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["robot_id"], name: "index_shipping_informations_on_robot_id"
    t.index ["shipment_id"], name: "index_shipping_informations_on_shipment_id"
  end

  add_foreign_key "reports", "robots"
  add_foreign_key "shipping_informations", "robots"
  add_foreign_key "shipping_informations", "shipments"
end
