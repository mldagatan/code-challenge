require 'quality_assurance_report'

def random_boolean
  [true, false].sample
end

def random_robot_statuses
  random = Robot::STATUSES.size

  Robot::STATUSES.sample(rand(0..random))
end

def create_report(robot, conclusion)
  report = robot.report || robot.build_report
  report.conclusion = conclusion

  report.save!
end

460.times do
  Robot.create(
    name:           Faker::Superhero.name,
    statuses:       random_robot_statuses,
    configuration:  {
      hasSentience:   random_boolean,
      hasTracks:      random_boolean,
      hasWheels:      random_boolean,
      numberOfRotors: rand(1..5),
      colour:         Faker::Color.color_name
    }
  )
end

robots = Robot.last(60)

recycle = robots[0..19]
seconds = robots[20..39]
passed = robots[40..59]

recycle.each { |robot| create_report(robot, 'recycle') }
seconds.each { |robot| create_report(robot, 'factory second') }
passed.each { |robot| create_report(robot, 'passed') }

for_shipment_passed = Robot.joins(:report)
                           .where(reports: { conclusion: 'passed' })

for_shipment_seconds = Robot.joins(:report)
                            .where(reports: { conclusion: 'factory second' })

robots_shipment_1 = for_shipment_passed[0..4] + for_shipment_seconds[0..4]
robots_shipment_2 = for_shipment_passed[5..9] + for_shipment_seconds[5..9]
robots_shipment_3 = for_shipment_passed[10..14] + for_shipment_seconds[10..14]

shipments = Shipment.create([
  { shipment_date: Date.tomorrow },
  { shipment_date: Date.tomorrow + 1.day },
  { shipment_date: Date.tomorrow + 2.days },
])

shipments.first.robots << robots_shipment_1
shipments.second.robots << robots_shipment_2
shipments.third.robots << robots_shipment_3
