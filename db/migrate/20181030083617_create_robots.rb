class CreateRobots < ActiveRecord::Migration[5.2]
  def change
    create_table :robots do |t|
      t.string  :name
      t.jsonb   :configuration
      t.string  :statuses,        array: true, default: []

      t.timestamps
    end
  end
end
