class CreateShipments < ActiveRecord::Migration[5.2]
  def change
    create_table :shipments do |t|
      t.string  :uuid
      t.date    :shipment_date,   default: ''
      t.string  :status,          default: 'ready'

      t.timestamps
    end
  end
end
