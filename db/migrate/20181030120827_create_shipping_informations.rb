class CreateShippingInformations < ActiveRecord::Migration[5.2]
  def change
    create_table :shipping_informations do |t|
      t.belongs_to :robot,    foreign_key: true
      t.belongs_to :shipment, foreign_key: true
      t.string     :status,   default: 'pending'

      t.timestamps
    end
  end
end
