class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.belongs_to :robot,  foreign_key: true
      t.string :conclusion, default: '', null: false

      t.timestamps
    end
  end
end
